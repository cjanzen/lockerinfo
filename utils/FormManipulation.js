// Function: addOptionToSelectBox
// Purpose: Appends a new option with the specified text, value, and selected
//   to the specified select box.
// Parameters:
//   text: the text for the option that will be displayed in the select box
//   value: the value of the option
//   selected: determines if the newly added option will be selected upon
//      addition
//   selectBoxId: the id of the select box that you want to add the option to
// Returns: Nothing
// Assumptions:
//   The “text”, “value”, and “selectBoxId” arguments are strings
//   The “selected” argument is set to true/false
//   The “selectBoxId” is set to point to an existing select box
function addOptionToSelectBox( text, value, selected, selectBoxId )
{
    // Holds a reference to the specified select box
    var eSelectBox;
    // Holds a new option object
    var opt;
    
    // Get a reference to the specified select box
    eSelectBox = document.getElementById( selectBoxId );
    // Create the new Option object with the specified text and value
    // properties (defaultSelected and selected are set to false)
    opt = new Option( text, value, false, false );
    // Add this option to the specified select box
    eSelectBox.add( opt, null );
    // If the “selected” argument was set to true
    if ( selected )
    {
        // Set the selectedIndex of the select box to the new option
        eSelectBox.selectedIndex = eSelectBox.length - 1;
    }
}


// Function: clearForm
// Purpose: Clears the specified form so that all options are set back to
//   their default settings (or blank if no default was specified)
// Parameters:
//   formId: The id of the form
// Returns: Nothing
// Assumptions:
//   The id points to an existing form element
//   The “formId” argument is a string
function clearForm( formId )
{
    // Clear the specified form with the reset method
    document.getElementById( formId ).reset();
}


// Function: updateTextBoxFromSelectBox
// Purpose: Updates the specified text box to reflect the currently selected
//   option in the specified select box
// Parameters:
//   textBoxId: The Id of the text box (as a string) that will be updated
//   selectBoxId: The Id of the select box (as a string, or can be specified
//      with “this” in an event handler) that this text box will reflect off
//      of.
// Returns: Nothing
// Assumptions:
//   The “textBoxId” argument will be an id that points to a text box input
//      element
//   The “selectBoxId” argument will be an id that points to a select box
//      input element (or "this" if the function is used within an event)
function updateTextBoxFromSelectBox( textBoxId, selectBoxId )
{
    // Holds a reference to the specified text box
    var eTextBox;
    // Holds a reference to the specified select box
    var eSelectBox;
    
    // Get a reference to the specified text box
    eTextBox = document.getElementById( textBoxId );
    // If “selectBoxId” is a type of string
    if ( typeof selectBoxId == "string" )
    {
        // Get a reference to the specified select box
        eSelectBox = document.getElementById( selectBoxId );
    }
    else // Else
    {
        // Set the reference of the specified select box to the eSelectBox
        // variable
        eSelectBox = selectBoxId;
    }
        
    // Set the value of the text box to the value of the select box
    eTextBox.value = eSelectBox.value;
}
