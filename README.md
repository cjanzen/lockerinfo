# LockerInfo

A very simple JavaScript application written for the Internet Program/Web Apps class. It's a simple form that allows you to store information about lock combinations for various students. It utilizes jQuery and jQuery Validation to do the form validation, as well as handle the logic of the form.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes.

### Prerequisites

You just need a web browser to view the "LockerInfo.html" file. Should work in Google Chrome, Microsoft Edge, and Mozilla Firefox.

### Installing

You just need to clone/download the files onto your system. Once downloaded, you can click on the "LockerInfo.html" file to run the application.

### Usage

The application contains two forms: one for adding a student with a locker combination and one for viewing a student with a locker combination.

When adding a student, you must provide a first name, a last name, and a lock combination in the format of NN-NN-NN, where NN is a one-digit or two-digit number between 0 and 59 inclusive. If you don't provide these pieces of information or enter the lock combination in the wrong format, an error message will appear on the respective field explaining the error. You cannot add a student unless all fields are filled in and valid. Clicking on the Add button adds the student to the form below and clicking Clear clears the form so that you can re-enter a student's information.

The bottom form allows you to view a student's lock combination by selecting a student's name from the drop-down box. Once selected, the student's lock combination will appear in the "Combination is" text box.

## Built With

* HTML/JavaScript/CSS - The languages used to develop the application.
* [jQuery](http://jquery.com/) - Javascript library used for document traversal and manipulation, event-handling, etc.
* [jQuery Validation](https://jqueryvalidation.org/) - Used for the client-side validation.

## Authors

* **Corey Janzen**  - [cjanzen](https://bitbucket.org/cjanzen/)


> Written with [StackEdit](https://stackedit.io/).